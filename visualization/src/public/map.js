function initMap(){
    var CartoDB_Voyager = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
	subdomains: 'abcd',
	maxZoom: 19
});

    var satellite = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash;'
    });

    var CartoDB_DarkMatter = L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
	subdomains: 'abcd',
	maxZoom: 19
    });


    var baseMaps = {
        "Voyage": CartoDB_Voyager,
        "Satellite": satellite,
        "Dark": CartoDB_DarkMatter

    };

    var map = L.map('map', {
        zoom: 9,
        maxZoom: 16,
        layers: [CartoDB_Voyager, satellite],
        timeDimension: true,
        timeDimensionOptions: {
            timeInterval: "2019-02-15T00:00:00.000Z/2019-02-16T00:00:00.000Z",
            period: "PT30M"
        },
        timeDimensionControl: true,
        timeDimensionControlOptions:{
            loopButton: true
        }
    });

    map.setView([-45.31218795776367, -73.555974121094]);

    return {
        map : map,
        baseMaps :baseMaps

    } 
}

var mapConf = initMap();
var map = mapConf.map;
var geoServer = 'http://191.235.79.57:8080/geoserver/geoOcean/gwc/service'

//GlobalVar
//default depth = 5 meters
var depthLayer = 'geoOcean:oceandata_5m'
var velocityLayer= null
var timeNow= '2019-02-15T00-00-00.000Z'

function createSalinityLayer(depthLay){
    var salinStyle=  L.tileLayer.wms(geoServer, {
        tiled:'true',
        layers : depthLay,
        transparent : true,
        styles : 'geoOcean:Salinity',
        format : 'image/png',
        SameSite: 'Strict'
        
    }); 
    return L.timeDimension.layer.wms(salinStyle)
}

function createTemperatureLayer(depthLay){
    var tempStyle = L.tileLayer.wms(geoServer, {
        tiled:'true',
        layers : depthLay, 
        transparent : true,
        styles : 'geoOcean:Temperature',
        format : 'image/png',
        SameSite: 'Strict'
    });
    return L.timeDimension.layer.wms(tempStyle)
}



function addWindLayer(){
    //Wind Layer
    var layer=depthLayer.split(':')[1]
    var currentFilename = '/jsonCurrentData/water-current_'+layer+'_'+timeNow+'.json'
    //se reemplaza el simbolo ':' por un '-', porque en la creacion de archivos no permite el simbolo ':'
    currentFilename = currentFilename.replace(/:/g,'-')
    $.getJSON(currentFilename, function(data){
        velocityLayer = L.velocityLayer({
        displayValues: false,
        displayOptions: {
          velocityType: "Water",
          position: "bottomleft",
          emptyString: "No water data",
        },
        data: data,
        minVelocity: 0.03,
        maxVelocity: 1.3,
        velocityScale: 0.1// arbitrary default 0.005
      });
      layerControl.addOverlay(velocityLayer, "Ocean Current", "Layers");
      
      
    });
}

// Layer cotrol
const options = {
    // Make the "Landmarks" group exclusive (use radio inputs)
    exclusiveGroups: ["Layers","Depth"],
    // Show a checkbox next to non-exclusive group labels for toggling all
    groupCheckboxes: true
    };

var layerControl = null;

//tiempo inicial 
function updateLayerControl(depthLay){
    var groupedOverlays = {
        "Layers": {
            "Temperature": createTemperatureLayer(depthLay),
            "Salinity": createSalinityLayer(depthLay), 
        }
    };
    layerControl = L.control.groupedLayers(mapConf.baseMaps, groupedOverlays, options).addTo(map);
    addWindLayer()
    addDepthsLayer()
    
}
updateLayerControl(depthLayer)

// leyendas de valores
var salLegend = L.control({
    position: 'bottomright'
});

var tempLegend = L.control({
    position: 'bottomright'
});

// layer legend
salLegend.onAdd = function(map) {
    var depthLay=depthLayer.replace(/:/g,'%3A')
    var url = "http://191.235.79.57:8080/geoserver/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer={}&style=Salinity";
    var src = url.format(depthLay)
    var div = L.DomUtil.create('div', 'info legend');
    div.innerHTML +=
        '<img src="' + src + '" alt="legend">';
    return div;
};

tempLegend.onAdd = function(map) {
    var depthLay=depthLayer.replace(/:/g,'%3A')
    var url = "http://191.235.79.57:8080/geoserver/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer={}&style=Temperature";
    var src = url.format(depthLay)
    var div = L.DomUtil.create('div', 'info legend');
    div.innerHTML +=
        '<img src="' + src + '" alt="legend">';
    return div;
};

map.on('overlayadd', function(eventLayer){
    if(eventLayer.name == 'Temperature'){
        tempLegend.addTo(this);
    }else if ( eventLayer.name == 'Salinity'){
        salLegend.addTo(this);
    }
});

map.on('overlayremove', function(eventLayer){
    if(eventLayer.name == 'Temperature'){
        map.removeControl(tempLegend);
  
    }else if (eventLayer.name == 'Salinity'){
        map.removeControl(salLegend);
    }
});

//depth conf
//para agregar algun elemento al control de capas debe ser parte del mapa por lo cual nos ayudaremos figuras



function addDepthsLayer(){
    var circle5 = L.circle([51.508, -0.11], {
        color: 'red',
        radius: 1
    })
    
    var circle10 = L.circle([51.508, -0.11], {
        color: 'red',
        radius: 1
    })
    
    var circle15 = L.circle([51.508, -0.11], {
        color: 'red',
        radius: 1
    })
    layerControl.addOverlay(circle5, "5 meters", "Depth");
    layerControl.addOverlay(circle10, "10 meters", "Depth");
    layerControl.addOverlay(circle15, "15 meters", "Depth");
}

//funciones que se activan al relizar una accion sobre e mapa
map.on('overlayadd', function(eventLayer){
    if(eventLayer.name=='5 meters'){
        depthLayer = 'geoOcean:oceandata_5m'
        layerControl.remove();
        map.invalidateSize()
        updateLayerControl(depthLayer);
    }else if(eventLayer.name == '10 meters'){
        depthLayer = 'geoOcean:oceandata_10m'
        layerControl.remove();
        updateLayerControl(depthLayer);
    }
    if(eventLayer.name == '15 meters'){
        depthLayer = 'geoOcean:oceandata_15m'
        layerControl.remove();
        updateLayerControl(depthLayer);
    }
});

map.timeDimension.on('timeloading', function(eventLayer) {
    //layerControl.removeLayer(velocityLayer)
        
        timeNow= timeConverter(map.timeDimension.getCurrentTime());
        addWindLayer(); 
});

function timeConverter(unixTimestamp){
    var date = new Date(unixTimestamp);
    return date.toISOString()
  }


// click
var popup = L.popup();

function onMapClick(e) {
    var lat = e.latlng['lat'];
    var lon = e.latlng['lng'];

    dataPoint(lat,lon).then(res =>{
        popup
        .setLatLng(e.latlng)
        .setContent(res)
        .openOn(map);
    });
}
map.on('click', onMapClick);

//funcion que permite utilizar la misma funcionalidad format que existe en python
String.prototype.format = function () {
    var i = 0, args = arguments;
    return this.replace(/{}/g, function () {
      return typeof args[i] != 'undefined' ? args[i++] : '';
    });
  };

//funcion obtiene los datos de un punto esoecifico del mapa
function dataPoint(lat, lon) {
    var altlat= lat+ 0.003
    var altlon= lon+ 0.003
    var depthLay=depthLayer.replace(/:/g,'%3A')
    var time=timeConverter(map.timeDimension.getCurrentTime()).replace(/:/g,'%3A')
    var url= "https://cors-anywhere.herokuapp.com/http://191.235.79.57:8080/geoserver/geoOcean/wms?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&FORMAT=image%2Fpng&QUERY_LAYERS={}&STYLES&LAYERS={}&exceptions=application%2Fvnd.ogc.se_inimage&TIME={}&INFO_FORMAT=text%2Fhtml&FEATURE_COUNT=50&X=50&Y=50&SRS=EPSG%3A4326&WIDTH=101&HEIGHT=101&BBOX={}%2C{}%2C{}%2C{}";
    var urlPoint=url.format(depthLay, depthLay, time, lon, lat, altlon, altlat)
   
    return fetch(urlPoint)
        .then((resp)=>{
            return resp.text()
            .then((data)=>{
                return oceanVarHumanText(data)});
        }).catch((err)=>{
            return 'No water Data'
        })
};

function dataRequest(url){
    
}

function oceanVarHumanText(data){
    let parser = new DOMParser(),
                xmlDoc = parser.parseFromString(data,'text/html')
                var atribute=(xmlDoc.getElementsByTagName('table')[0].getElementsByTagName('tbody')[0]).getElementsByTagName('tr')[0].getElementsByTagName('th');
                var value=(xmlDoc.getElementsByTagName('table')[0].getElementsByTagName('tbody')[0]).getElementsByTagName('tr')[1].getElementsByTagName('td');
                var depth= (depthLayer.split('_')[1]).slice(0, -1)
                return '<b>'+atribute[3].innerText+':</b> '+value[3].innerText+'<br><b>'+atribute[4].innerText+':</b> '+value[4].innerText+'<br><b>'+atribute[5].innerText+':</b> '+value[5].innerText+'<br><b>'+'Depth:</b> '+depth+' meters'+'<br><b>'+atribute[6].innerText+':</b> '+value[6].innerText; 
}