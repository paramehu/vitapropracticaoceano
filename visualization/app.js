const express = require ('express')
const app  = express();

//settings
app.set('port', 9090);

// route
app.get('/', (req, res)=> {
	res.sendFile(__dirname + '/src/view/index.html')
})

//static files
app.use(express.static(__dirname+'/src/public'));
app.use('/scripts',express.static(__dirname+'/node_modules'));

//server listener

app.listen(app.get('port'), ()=> {
	console.log('Server on port  ', app.get('port'));
});